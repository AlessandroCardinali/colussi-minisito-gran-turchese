// http://turnjs.com/docs/Main_Page

function jqUpdateSizeCard(){
	// Get the dimensions of the viewport
	var width = $(window).width();
	
	if (width <= 768) {
		wCard = 300;
		hCard = 220;
	} else {
		wCard = 600;
		hCard = 440;
	}
};
$(document).ready(jqUpdateSizeCard);	// When the page first loads
$(window).resize(jqUpdateSizeCard);	 // When the browser changes size

$(window).ready(function() {
		
	$('#card').turn({
		width: wCard,
	  height: hCard,
		display: 'double',
		acceleration: true,
		gradients: !$.isTouch,
		elevation:50,
		// autoCenter: true,
		when: {
			turning: function(e, page) {
				// event.preventDefault();
				$('.intro-carta').hide();
			}
		}	
	});
	
	$("#btnBang").on("click", function(e){
		// e.preventDefault();
		$("#card").turn("page", 2);
	});
	$("#btnBang-sm").on("click", function(e){
		// e.preventDefault();
		$("#card").turn("page", 2);
	});
	
});

$(window).bind('keydown', function(e){
	
	if (e.keyCode==37)
		$('#card').turn('previous');
	else if (e.keyCode==39)
		$('#card').turn('next');
		
});