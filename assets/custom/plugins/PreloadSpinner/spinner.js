// Loader spinner
// http://stackoverflow.com/questions/9734021/jquery-hide-content-until-loaded
// http://tobiasahlin.com/spinkit/
$(window).load(function() {
  // When the page has loaded
  $(".content-fade-in").fadeOut(600);
});